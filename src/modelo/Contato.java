package modelo;

public class Contato extends Pessoa {

	private String localTrabalho;
	private String telefoneComercial;
	
	public Contato(String nome) {
		super(nome);
	}
	
	public String getLocalTrabalho() {
		return localTrabalho;
	}

	public void setLocalTrabalho(String localTrabalho) {
		this.localTrabalho = localTrabalho;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	
	
}
