package modelo;

public class Amigos extends Pessoa {

	private String aniversario;
	private String facebook;
	
	public Amigos(String nome) {
		super(nome);
	}

	public String getAniversario() {
		return aniversario;
	}

	public void setAniversario(String aniversario) {
		this.aniversario = aniversario;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	
}
