package visao;

import java.util.Scanner;

import modelo.Amigo;

public class MinhaAgenda {

	public static void main(String[] args) {
		Scanner leitor = new Scanner(System.in);
		
		Amigo umAmigo;
		
		System.out.println("Digite o nome: ");
		String umNome = leitor.nextLine();
		
		umAmigo = new Amigo(umNome);
		
		System.out.println("Digite o telefone: ");
		String umTelefone = leitor.nextLine();
		umAmigo.setTelefone(umTelefone);
		
		System.out.println("Digite o email: ");
		String umEmail = leitor.nextLine();
		umAmigo.setEmail(umEmail);
		
		System.out.println("Digite o aniversário: ");
		String umAniversario = leitor.nextLine();
		umAmigo.setAniversario(umAniversario);
		
		System.out.println("Digite o facebook: ");
		String umFacebook = leitor.nextLine();
		umAmigo.setFacebook(umFacebook);
		
		System.out.println("Nome: " + umAmigo.getNome());
		System.out.println("Telefone: " + umAmigo.getTelefone());
		System.out.println("Email: " + umAmigo.getEmail());
		System.out.println("Aniversario: " + umAmigo.getAniversario());
		System.out.println("Facebook: " + umAmigo.getFacebook());
		leitor.close();
	}

}
